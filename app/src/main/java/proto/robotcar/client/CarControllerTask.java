package proto.robotcar.client;

import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import io.grpc.ManagedChannelBuilder;
import io.grpc.ManagedChannel;
import proto.robotcar.service.api.CarControllerServiceGrpc;
import proto.robotcar.service.api.EmptyRequest;

/**
 * Created by torstenmosis on 02.11.17.
 */

public class CarControllerTask extends AsyncTask<String, Void, String> {

    private final String mHost;
    private final int mPort;
    private ManagedChannel mChannel;
    private static final String TAG = "CarControllerTask";


    public CarControllerTask(String host, int port) {
        mHost = host;
        mPort = port;
    }

    @Override
    protected void onPreExecute() {
        mChannel = ManagedChannelBuilder.forAddress(mHost, mPort)
                .usePlaintext(true)
                .build();
    }

    @Override
    protected void onPostExecute(String result) {

        MainActivity.getInstace().updateStartStop(result);

        try {
            mChannel.shutdown().awaitTermination(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

    }

    @Override
    protected String doInBackground(String... params) {

        String result = "Unknown";

        if(mChannel != null) {
            CarControllerServiceGrpc.CarControllerServiceBlockingStub proxy = CarControllerServiceGrpc.newBlockingStub(mChannel);
            EmptyRequest request = EmptyRequest.newBuilder().build();
            if (params[0].equals("Start")) {
                Log.i(TAG, "Start");
                proxy.start(request).getSuccess();
                result = "Stop";
            }
            else if (params[0].equals("Stop")){
                Log.i(TAG, "Stop");
                proxy.stop(request).getSuccess();
                result = "Start";
            }
            else {
                Log.w(TAG, "Unknown command: "+params[0]);
            }
        }

        return result;
    }

}
