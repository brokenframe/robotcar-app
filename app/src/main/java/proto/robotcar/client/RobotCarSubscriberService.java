package proto.robotcar.client;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

import io.grpc.ConnectivityState;
import io.grpc.ManagedChannelBuilder;
import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;
import proto.robotcar.service.api.CarDataProviderServiceGrpc;
import proto.robotcar.service.api.CarValueData;
import proto.robotcar.service.api.ClientId;
import proto.robotcar.service.api.EmptyMessage;
import proto.robotcar.service.api.Subscription;
import proto.robotcar.service.api.TrafficLightControllerServiceGrpc;
import proto.robotcar.service.api.TrafficLightState;

/**
 * Created by torstenmosis on 02.11.17.
 */

public class RobotCarSubscriberService extends Service {

    private ManagedChannel mChannel;
    private static final String TAG = "SubscriberService";
    public static final String TRAFFIC_LIGHT_UPDATE_ACTION = "proto.robotcar.client.TRAFFIC_LIGHT_UPDATE_ACTION";
    public static final String TRAFFIC_LIGHT_VALUE = "trafficlightvalue";

    public static final String CONNECTION_ACTION = "proto.robotcar.client.CONNECTION_ACTION";
    public static final String CONNECTION_STATE = "connectionstate";

    public static final int CONNECTION_STATE_UNKNOWN = -1;
    public static final int CONNECTED = 1;
    public static final int DISCONNECTED = 2;
    public static final int CONNECTION_BROKEN = 3;
    public static final int CONNECTION_NOT_READY = 4;

    private boolean connected = false;

    TrafficLightControllerServiceGrpc.TrafficLightControllerServiceStub tiProxy;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Log.i(TAG, "Start service listening to RobotCarServer");

        String host = intent.getStringExtra("host");
        int port = intent.getIntExtra("port", 50051);

        mChannel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext(true)
                .build();
/*
        mChannel.notifyWhenStateChanged(mChannel.getState(true), new Runnable() {
            @Override
            public void run() {
                ConnectivityState newState = mChannel.getState(true);
                Log.i(TAG, "");
            }
        });
*/
        // Workaround
        mChannel.getState(true);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(mChannel.getState(true).equals(ConnectivityState.READY)) {
            publishResults(CONNECTION_ACTION, CONNECTION_STATE, CONNECTED);
            subscribeOnTrafficLightChanges();
            connected = true;
        }

        else {
            publishResults(CONNECTION_ACTION, CONNECTION_STATE, CONNECTION_NOT_READY);
        }

        return START_STICKY;
    }


    private void subscribeOnTrafficLightChanges() {

        Log.i(TAG, "Subscribe on Traffic Light Changes");

        tiProxy = TrafficLightControllerServiceGrpc.newStub(mChannel);
        ClientId request = ClientId.newBuilder().setName("AndroidClient").build();

        tiProxy.subscribeOnTrafficLightState(request, new StreamObserver<TrafficLightState>() {
            @Override
            public void onNext(proto.robotcar.service.api.TrafficLightState value) {
                Log.i(TAG, value.getState().toString());
                publishResults(TRAFFIC_LIGHT_UPDATE_ACTION, TRAFFIC_LIGHT_VALUE, value.getStateValue());
            }

            @Override
            public void onError(Throwable t) {
                Log.i(TAG, "Error: Disconnected unexpected");
                publishResults(CONNECTION_ACTION, CONNECTION_STATE, CONNECTION_BROKEN);
            }

            @Override
            public void onCompleted() {
                Log.i(TAG, "Subscription for Traffic Light Changes ended");
            }
        });

    }

    private void unSubscribeOnTrafficLightChanges() {

        Log.i(TAG, "Unsubscribe on Traffic Light Changes");

        ClientId request = ClientId.newBuilder().setName("AndroidClient").build();
        tiProxy.unsubscribeOnTrafficLightState(request, new StreamObserver<EmptyMessage>() {
            @Override
            public void onNext(EmptyMessage value) {
                Log.i(TAG, "unSubscribeOnTrafficLightChanges acknowledged");
            }

            @Override
            public void onError(Throwable t) {
            }

            @Override
            public void onCompleted() {
                Log.i(TAG, "unSubscribeOnTrafficLightChanges completed");
            }
        });
    }

    private void subscribeOnCarDataChanges() {

        Log.i(TAG, "Subscribe on Car Data Changes");

        CarDataProviderServiceGrpc.CarDataProviderServiceStub cdProxy = CarDataProviderServiceGrpc.newStub(mChannel);
        Subscription cdRegister = Subscription.newBuilder().setType(Subscription.Type.REGISTER).build();

        cdProxy.subscribeOnCarValueData(cdRegister, new StreamObserver<CarValueData>() {

            @Override
            public void onNext(CarValueData value) {
                Log.i(TAG, value.getDrivingDirection());
                Log.i(TAG, value.getDrivingState());
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {

            }
        });

    }



    @Override
    public void onDestroy() {

        Log.i(TAG, "Stop service listening to RobotCarServer");

        if(connected) {
            unSubscribeOnTrafficLightChanges();
            try {
                mChannel.shutdown().awaitTermination(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            publishResults(CONNECTION_ACTION, CONNECTION_STATE, DISCONNECTED);
        }

        super.onDestroy();

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void publishResults(String intentId, String key, int value) {
        Log.i(TAG, "Broadcasting back to activity "+intentId+", "+key+", "+value);
        Intent intent = new Intent(intentId);
        intent.putExtra(key, value);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
