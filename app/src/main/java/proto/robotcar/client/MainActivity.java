package proto.robotcar.client;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.net.URI;

import io.grpc.ManagedChannel;
import proto.robotcar.service.api.TrafficLightState;

public class MainActivity extends AppCompatActivity {


    private static final String TAG = "RobotCarApp";
    private URI serverURI;


    private boolean started = false;
    private ManagedChannel mChannel;
    private static MainActivity mainActivityRunningInstance;
    private Intent intent;

    public static MainActivity  getInstace(){
        return mainActivityRunningInstance;
    }

    public void updateTrafficLight(final String str) {
        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                ImageView imgView = (ImageView) findViewById(R.id.trafficLightView);
                if(str.equals("RED")) {
                    imgView.setImageResource(R.drawable.traffic_red);
                }
                else if(str.equals("REDYELLOW")) {
                    imgView.setImageResource(R.drawable.traffic_redyellow);
                }
                else if(str.equals("YELLOW")) {
                    imgView.setImageResource(R.drawable.traffic_yellow);
                }
                else if(str.equals("GREEN")) {
                    imgView.setImageResource(R.drawable.traffic_green);
                }
                else  {
                    imgView.setImageResource(R.drawable.traffic_off);
                }
            }
        });
    }

    public void updateStartStop(final String str) {
        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {
                Button startStopButton = (Button) findViewById(R.id.start_stop);
                startStopButton.setText(str);
                startStopButton.invalidate();
            }
        });
    }

    public void updateConnect(final int value) {
        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {

                Button connectButton = (Button) findViewById(R.id.connect);

                switch (value) {
                    case RobotCarSubscriberService.CONNECTED:
                        connectButton.setText("Disconnect");
                        connectButton.invalidate();
                        break;
                    case RobotCarSubscriberService.DISCONNECTED:
                        connectButton.setText("Connect");
                        connectButton.invalidate();
                        updateTrafficLight("OFF");
                        break;
                    case RobotCarSubscriberService.CONNECTION_BROKEN:
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.connection_lost), Toast.LENGTH_LONG).show();
                        stopService(intent);
                        connectButton.setText("Connect");
                        connectButton.invalidate();
                        updateTrafficLight("OFF");
                        break;
                    case RobotCarSubscriberService.CONNECTION_NOT_READY:
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.not_ready), Toast.LENGTH_LONG).show();
                        stopService(intent);
                        break;
                    default:
                }

            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityRunningInstance = this;

        Button connectButton = (Button) findViewById(R.id.connect);
        connectButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(connectButton.getText().equals("Connect")) {
                    Log.i(TAG, "Connect to RobotCar Server");
                    intent = new Intent(getApplicationContext(), RobotCarSubscriberService.class);
                    intent.putExtra("host", getHost());
                    intent.putExtra("port", getPort());
                    startService(intent);
                }
                else {
                    Log.i(TAG, "Disconnect from RobotCar Server");
                    stopService(intent);
                }
            }
        });

        Button startStopButton = (Button) findViewById(R.id.start_stop);
        startStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String command = startStopButton.getText().toString();
                CarControllerTask task = new CarControllerTask(getHost(), getPort());
                task.execute(command);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "Register local broadcast");
        // register local broadcast
        IntentFilter filter = new IntentFilter();
        filter.addAction(RobotCarSubscriberService.CONNECTION_ACTION);
        filter.addAction(RobotCarSubscriberService.TRAFFIC_LIGHT_UPDATE_ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, filter);
    }

    /**
     * Broadcast receiver to receive the data
     */
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(RobotCarSubscriberService.CONNECTION_ACTION)) {
                updateConnect(intent.getIntExtra(RobotCarSubscriberService.CONNECTION_STATE, RobotCarSubscriberService.CONNECTION_STATE_UNKNOWN));
            }
            else if(intent.getAction().equals(RobotCarSubscriberService.TRAFFIC_LIGHT_UPDATE_ACTION)) {
                int value = intent.getIntExtra(RobotCarSubscriberService.TRAFFIC_LIGHT_VALUE, TrafficLightState.State.RED.getNumber());
                String name = TrafficLightState.State.values()[value].name();
                updateTrafficLight(name);
            }
        }

    };

    private String getHost() {
        EditText serverIP = (EditText) findViewById(R.id.server);
        return serverIP.getText().toString().split(":")[0];
    }

    private int getPort() {
        EditText serverIP = (EditText) findViewById(R.id.server);
        return Integer.parseInt(serverIP.getText().toString().split(":")[1]);
    }

}
